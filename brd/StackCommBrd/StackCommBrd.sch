EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2350 3850 2350 3900
Wire Wire Line
	1750 3800 1750 3850
Wire Wire Line
	1750 3850 1850 3850
$Comp
L power:GND #PWR0101
U 1 1 6E5A568F
P 2350 4500
F 0 "#PWR0101" H 2350 4250 50  0001 C CNN
F 1 "GND" H 2355 4327 50  0000 C CNN
F 2 "" H 2350 4500 50  0001 C CNN
F 3 "" H 2350 4500 50  0001 C CNN
	1    2350 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6E5A5B89
P 1850 4500
F 0 "#PWR0102" H 1850 4250 50  0001 C CNN
F 1 "GND" H 1855 4327 50  0000 C CNN
F 2 "" H 1850 4500 50  0001 C CNN
F 3 "" H 1850 4500 50  0001 C CNN
	1    1850 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 6E5A5EB1
P 2350 3200
F 0 "#PWR0103" H 2350 3050 50  0001 C CNN
F 1 "+24V" H 2365 3373 50  0000 C CNN
F 2 "" H 2350 3200 50  0001 C CNN
F 3 "" H 2350 3200 50  0001 C CNN
	1    2350 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0104
U 1 1 6E5A6291
P 1850 3200
F 0 "#PWR0104" H 1850 3050 50  0001 C CNN
F 1 "+24V" H 1865 3373 50  0000 C CNN
F 2 "" H 1850 3200 50  0001 C CNN
F 3 "" H 1850 3200 50  0001 C CNN
	1    1850 3200
	1    0    0    -1  
$EndComp
Text Label 2650 3800 0    50   ~ 0
canh
Wire Wire Line
	1750 3800 3500 3800
Wire Wire Line
	3500 3800 3500 3850
Wire Wire Line
	4000 3900 4000 3850
Wire Wire Line
	6100 3800 6100 3850
Connection ~ 3500 3800
Wire Wire Line
	6600 3900 6600 3850
$Comp
L power:+24V #PWR0105
U 1 1 6E5B5261
P 3500 3200
F 0 "#PWR0105" H 3500 3050 50  0001 C CNN
F 1 "+24V" H 3515 3373 50  0000 C CNN
F 2 "" H 3500 3200 50  0001 C CNN
F 3 "" H 3500 3200 50  0001 C CNN
	1    3500 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0106
U 1 1 6E5B56B3
P 4000 3200
F 0 "#PWR0106" H 4000 3050 50  0001 C CNN
F 1 "+24V" H 4015 3373 50  0000 C CNN
F 2 "" H 4000 3200 50  0001 C CNN
F 3 "" H 4000 3200 50  0001 C CNN
	1    4000 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0107
U 1 1 6E5B5A85
P 6100 3200
F 0 "#PWR0107" H 6100 3050 50  0001 C CNN
F 1 "+24V" H 6115 3373 50  0000 C CNN
F 2 "" H 6100 3200 50  0001 C CNN
F 3 "" H 6100 3200 50  0001 C CNN
	1    6100 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0108
U 1 1 6E5B5EA8
P 6600 3200
F 0 "#PWR0108" H 6600 3050 50  0001 C CNN
F 1 "+24V" H 6615 3373 50  0000 C CNN
F 2 "" H 6600 3200 50  0001 C CNN
F 3 "" H 6600 3200 50  0001 C CNN
	1    6600 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 6E5B6A30
P 3500 4500
F 0 "#PWR0111" H 3500 4250 50  0001 C CNN
F 1 "GND" H 3505 4327 50  0000 C CNN
F 2 "" H 3500 4500 50  0001 C CNN
F 3 "" H 3500 4500 50  0001 C CNN
	1    3500 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 6E5B6D86
P 4000 4500
F 0 "#PWR0112" H 4000 4250 50  0001 C CNN
F 1 "GND" H 4005 4327 50  0000 C CNN
F 2 "" H 4000 4500 50  0001 C CNN
F 3 "" H 4000 4500 50  0001 C CNN
	1    4000 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 6E5B7076
P 6100 4500
F 0 "#PWR0113" H 6100 4250 50  0001 C CNN
F 1 "GND" H 6105 4327 50  0000 C CNN
F 2 "" H 6100 4500 50  0001 C CNN
F 3 "" H 6100 4500 50  0001 C CNN
	1    6100 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 6E5B73DD
P 6600 4500
F 0 "#PWR0114" H 6600 4250 50  0001 C CNN
F 1 "GND" H 6605 4327 50  0000 C CNN
F 2 "" H 6600 4500 50  0001 C CNN
F 3 "" H 6600 4500 50  0001 C CNN
	1    6600 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J2
U 1 1 62A239EC
P 3700 3850
F 0 "J2" H 3750 4367 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 3750 4276 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x08_P2.54mm_Vertical" H 3700 3850 50  0001 C CNN
F 3 "~" H 3700 3850 50  0001 C CNN
	1    3700 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3200 3500 3550
Wire Wire Line
	4000 3200 4000 3550
Wire Wire Line
	6100 3200 6100 3550
Wire Wire Line
	6600 3200 6600 3550
Wire Wire Line
	3500 3650 3500 3750
Wire Wire Line
	3500 4250 3500 4500
Wire Wire Line
	4000 4250 4000 4500
Wire Wire Line
	4000 3650 4000 3750
Wire Wire Line
	4000 3650 4000 3550
Connection ~ 4000 3650
Connection ~ 4000 3550
Wire Wire Line
	3500 3650 3500 3550
Connection ~ 3500 3650
Connection ~ 3500 3550
Connection ~ 3500 4250
Wire Wire Line
	3500 4050 3500 4150
Connection ~ 3500 4150
Wire Wire Line
	3500 4150 3500 4250
Connection ~ 4000 4250
Wire Wire Line
	4000 4050 4000 4150
Connection ~ 4000 4150
Wire Wire Line
	4000 4150 4000 4250
$Comp
L Connector_Generic:Conn_02x08_Odd_Even J3
U 1 1 62A2BA86
P 6300 3850
F 0 "J3" H 6350 4367 50  0000 C CNN
F 1 "Conn_02x08_Odd_Even" H 6350 4276 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x08_P2.54mm_Horizontal" H 6300 3850 50  0001 C CNN
F 3 "~" H 6300 3850 50  0001 C CNN
	1    6300 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3650 6100 3750
Wire Wire Line
	6100 4250 6100 4500
Wire Wire Line
	6600 4250 6600 4500
Wire Wire Line
	6600 3650 6600 3750
Wire Wire Line
	6100 3650 6100 3550
Connection ~ 6100 3650
Connection ~ 6100 3550
Wire Wire Line
	6600 3650 6600 3550
Connection ~ 6600 3650
Connection ~ 6600 3550
Wire Wire Line
	6100 4250 6100 4150
Connection ~ 6100 4250
Connection ~ 6100 4150
Wire Wire Line
	6100 4150 6100 4050
Wire Wire Line
	6600 4250 6600 4150
Connection ~ 6600 4250
Connection ~ 6600 4150
Wire Wire Line
	6600 4150 6600 4050
Wire Wire Line
	3500 3800 6100 3800
Wire Wire Line
	4000 3900 6600 3900
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6380F870
P 2050 3850
F 0 "J1" H 2100 4267 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2100 4176 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Horizontal" H 2050 3850 50  0001 C CNN
F 3 "~" H 2050 3850 50  0001 C CNN
	1    2050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3750 1850 3650
Connection ~ 1850 3650
Wire Wire Line
	1850 3650 1850 3200
Wire Wire Line
	2350 3750 2350 3650
Connection ~ 2350 3650
Wire Wire Line
	2350 3650 2350 3200
Wire Wire Line
	1850 3950 1850 4050
Connection ~ 1850 4050
Wire Wire Line
	1850 4050 1850 4500
Wire Wire Line
	2350 3950 2350 4050
Connection ~ 2350 4050
Wire Wire Line
	2350 4050 2350 4500
NoConn ~ 4000 3950
NoConn ~ 3500 3950
Wire Wire Line
	2350 3900 4000 3900
Connection ~ 4000 3900
NoConn ~ 6600 3950
NoConn ~ 6100 3950
$EndSCHEMATC
