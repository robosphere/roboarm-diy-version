EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2350 3850 2350 3900
Wire Wire Line
	1750 3800 1750 3850
Wire Wire Line
	1750 3850 1850 3850
$Comp
L power:GND #PWR0101
U 1 1 6E5A568F
P 2350 4500
F 0 "#PWR0101" H 2350 4250 50  0001 C CNN
F 1 "GND" H 2355 4327 50  0000 C CNN
F 2 "" H 2350 4500 50  0001 C CNN
F 3 "" H 2350 4500 50  0001 C CNN
	1    2350 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6E5A5B89
P 1850 4500
F 0 "#PWR0102" H 1850 4250 50  0001 C CNN
F 1 "GND" H 1855 4327 50  0000 C CNN
F 2 "" H 1850 4500 50  0001 C CNN
F 3 "" H 1850 4500 50  0001 C CNN
	1    1850 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0103
U 1 1 6E5A5EB1
P 2350 3200
F 0 "#PWR0103" H 2350 3050 50  0001 C CNN
F 1 "+24V" H 2365 3373 50  0000 C CNN
F 2 "" H 2350 3200 50  0001 C CNN
F 3 "" H 2350 3200 50  0001 C CNN
	1    2350 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0104
U 1 1 6E5A6291
P 1850 3200
F 0 "#PWR0104" H 1850 3050 50  0001 C CNN
F 1 "+24V" H 1865 3373 50  0000 C CNN
F 2 "" H 1850 3200 50  0001 C CNN
F 3 "" H 1850 3200 50  0001 C CNN
	1    1850 3200
	1    0    0    -1  
$EndComp
Text Label 2650 3800 0    50   ~ 0
canh
Text Label 2650 3900 0    50   ~ 0
canl
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 6380F870
P 2050 3850
F 0 "J1" H 2100 4267 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 2100 4176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2050 3850 50  0001 C CNN
F 3 "~" H 2050 3850 50  0001 C CNN
	1    2050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3750 1850 3650
Connection ~ 1850 3650
Wire Wire Line
	1850 3650 1850 3200
Wire Wire Line
	2350 3750 2350 3650
Connection ~ 2350 3650
Wire Wire Line
	2350 3650 2350 3200
Wire Wire Line
	1850 3950 1850 4050
Connection ~ 1850 4050
Wire Wire Line
	1850 4050 1850 4500
Wire Wire Line
	2350 3950 2350 4050
Connection ~ 2350 4050
Wire Wire Line
	2350 4050 2350 4500
$Comp
L Connector:Conn_01x06_Female J3
U 1 1 63860F30
P 4250 4100
F 0 "J3" H 4278 4076 50  0000 L CNN
F 1 "Conn_01x06_Female" H 4278 3985 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical_SMD_Pin1Right" H 4250 4100 50  0001 C CNN
F 3 "~" H 4250 4100 50  0001 C CNN
	1    4250 4100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Male J2
U 1 1 63861857
P 4250 3400
F 0 "J2" H 4222 3282 50  0000 R CNN
F 1 "Conn_01x06_Male" H 4222 3373 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical_SMD_Pin1Right" H 4250 3400 50  0001 C CNN
F 3 "~" H 4250 3400 50  0001 C CNN
	1    4250 3400
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x06_Male J5
U 1 1 6386379F
P 5400 4100
F 0 "J5" H 5508 4481 50  0000 C CNN
F 1 "Conn_01x06_Male" H 5508 4390 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical_SMD_Pin1Right" H 5400 4100 50  0001 C CNN
F 3 "~" H 5400 4100 50  0001 C CNN
	1    5400 4100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Female J4
U 1 1 638649C0
P 5400 3400
F 0 "J4" H 5292 2875 50  0000 C CNN
F 1 "Conn_01x06_Female" H 5292 2966 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical_SMD_Pin1Right" H 5400 3400 50  0001 C CNN
F 3 "~" H 5400 3400 50  0001 C CNN
	1    5400 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 3650 3800 3900
Wire Wire Line
	3800 3900 4050 3900
Wire Wire Line
	5800 3750 5800 3900
Wire Wire Line
	5800 3900 5600 3900
Wire Wire Line
	1750 3800 3150 3800
Wire Wire Line
	5600 3500 6050 3500
Wire Wire Line
	6050 3500 6050 3400
Wire Wire Line
	5600 3400 6050 3400
Connection ~ 6050 3400
Wire Wire Line
	6050 3400 6050 3300
Wire Wire Line
	5600 3300 6050 3300
Connection ~ 6050 3300
Wire Wire Line
	6050 3300 6050 3200
Wire Wire Line
	5600 3200 6050 3200
Wire Wire Line
	4050 4400 3600 4400
Wire Wire Line
	4050 4000 3600 4000
Wire Wire Line
	3600 4000 3600 4100
Wire Wire Line
	4050 4300 3600 4300
Connection ~ 3600 4300
Wire Wire Line
	3600 4300 3600 4400
Wire Wire Line
	4050 4200 3600 4200
Connection ~ 3600 4200
Wire Wire Line
	3600 4200 3600 4300
Wire Wire Line
	4050 4100 3600 4100
Connection ~ 3600 4100
Wire Wire Line
	3600 4100 3600 4200
$Comp
L power:+24V #PWR0105
U 1 1 6386C6FF
P 5900 2850
F 0 "#PWR0105" H 5900 2700 50  0001 C CNN
F 1 "+24V" H 5915 3023 50  0000 C CNN
F 2 "" H 5900 2850 50  0001 C CNN
F 3 "" H 5900 2850 50  0001 C CNN
	1    5900 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0106
U 1 1 6386CE55
P 3650 2950
F 0 "#PWR0106" H 3650 2800 50  0001 C CNN
F 1 "+24V" H 3665 3123 50  0000 C CNN
F 2 "" H 3650 2950 50  0001 C CNN
F 3 "" H 3650 2950 50  0001 C CNN
	1    3650 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 6386D531
P 6050 3650
F 0 "#PWR0107" H 6050 3400 50  0001 C CNN
F 1 "GND" H 6055 3477 50  0000 C CNN
F 2 "" H 6050 3650 50  0001 C CNN
F 3 "" H 6050 3650 50  0001 C CNN
	1    6050 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 6386DB27
P 3600 4500
F 0 "#PWR0108" H 3600 4250 50  0001 C CNN
F 1 "GND" H 3605 4327 50  0000 C CNN
F 2 "" H 3600 4500 50  0001 C CNN
F 3 "" H 3600 4500 50  0001 C CNN
	1    3600 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4000 5900 4000
Wire Wire Line
	5900 4000 5900 4100
Wire Wire Line
	5600 4400 5900 4400
Wire Wire Line
	5600 4300 5900 4300
Connection ~ 5900 4300
Wire Wire Line
	5900 4300 5900 4400
Wire Wire Line
	5600 4200 5900 4200
Connection ~ 5900 4200
Wire Wire Line
	5900 4200 5900 4300
Wire Wire Line
	5600 4100 5900 4100
Connection ~ 5900 4100
Wire Wire Line
	5900 4100 5900 4200
Wire Wire Line
	4050 3200 3650 3200
Wire Wire Line
	3650 3200 3650 3300
Wire Wire Line
	4050 3300 3650 3300
Connection ~ 3650 3300
Wire Wire Line
	3650 3300 3650 3400
Wire Wire Line
	4050 3400 3650 3400
Connection ~ 3650 3400
Wire Wire Line
	3650 3400 3650 3500
Wire Wire Line
	3800 3900 3150 3900
Wire Wire Line
	3150 3900 3150 3800
Connection ~ 3800 3900
Wire Wire Line
	2350 3900 3000 3900
Wire Wire Line
	3000 3900 3000 3750
Wire Wire Line
	3000 3750 3300 3750
Wire Wire Line
	3300 3100 3300 3750
Wire Wire Line
	3300 3100 4050 3100
Connection ~ 3300 3750
Wire Wire Line
	3300 3750 5800 3750
Wire Wire Line
	3650 2950 3650 3200
Connection ~ 3650 3200
Wire Wire Line
	3600 4400 3600 4500
Connection ~ 3600 4400
Wire Wire Line
	3650 3500 3650 3600
Connection ~ 3650 3500
Wire Wire Line
	3650 3600 4050 3600
Wire Wire Line
	3650 3500 4050 3500
Wire Wire Line
	5900 2850 5900 4000
Connection ~ 5900 4000
Wire Wire Line
	6050 3650 6050 3600
Connection ~ 6050 3500
Wire Wire Line
	5150 3650 5150 3050
Wire Wire Line
	5150 3050 5800 3050
Wire Wire Line
	5800 3050 5800 3100
Wire Wire Line
	5800 3100 5600 3100
Wire Wire Line
	5150 3650 3800 3650
Wire Wire Line
	5600 3600 6050 3600
Connection ~ 6050 3600
Wire Wire Line
	6050 3600 6050 3500
$EndSCHEMATC
