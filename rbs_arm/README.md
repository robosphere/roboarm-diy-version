# Robossembler Arm Description Package

This ROS2 package includes the 'robot_description' of 'Robossembler Arm'.

The model is given as a xacro file to which any gripper by gripper name can be attached.

## Launch files Directory

To get additional argument's description use `ros2 launch rbs_arm <*.launch.py> --show-args`.

- `control.launch.py` - To launch ros2_control controllers
- `display.launch.py` - To show robot in RViz
- `display_gazebosim.launch.py` - To show robot in RViz & gazebo
- `get_urdf.launch.py` - To generate URDF file and save result as `current.urdf` in current directory
- `moveit.launch.py` - To run MoveIt2
- `moveit_gazebosim.launch.py` - To run MoveIt2, Gazebo, RViz
