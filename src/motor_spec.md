# Roboarm DIY Version

22.02.18  дополнена схема процессорного модуля изменен процессор с stm32f205 на  stm32f446ret, вместо которого можно впаивать более производительный кристал stm32f730r8t или подобные с корпусом lqfp64 
arm v06

![roboarm_diy_version](img/arm_v06.png)

arm v05

![roboarm_diy_version](img/arm_v05.png)

motor v01

![roboarm_diy_version](img/motor_220117.png)

arm v04

![roboarm_diy_version](img/arm_v04.png)

Вариант без проводов, только контактные группы

![roboarm_diy_version](img/arm_v03.png)


Roboarm with cheap mass market servos

![roboarm_diy_version](img/arm.png)

## Двигатель GM6208

### Описание

Двигатель серии GM62 от iPower Motors - это бесщеточный карданный двигатель для камер DSLR / CANON 5D MARKII, MARKIII. Этот мотор предназначен для крупномасштабных многовинтовых платформ, которые предназначены для подъема редукторов Red Epic & DSLR размером с-крутящий момент 4 кг/см.

### Характеристики

* Номер модели : GM6208
* Вес: 265,4 г
* Размеры двигателя: dia 69,5 мм x hgt 24 мм
* Размеры статора: диаметр 62 мм x hgt 8 мм
* Медный провод (OD): 0,25 мм
* Конфигурация: 24N28P
* Сопротивление: 32,1 Ом
* Предварительно намотанный: с 150 оборотами, полый вал
* Диаметр полого вала: 22 мм
* Диапазон камеры: 950-2300 грамм
* Испытательное напряжение (максимальное напряжение): 11,1 В (22,2 в)
* Тестовый ток: 0,086a
* Максимальный ток: 10А
* Тестовая Скорость вращения (макс): 278 об/мин (560 об/мин)
* Цена: $60 ([aliexpress](https://aliexpress.ru/item/4000329789618.html?spm=a2g2w.productlist.0.0.78d82e58m4mgWi))

### Габариты

![gm6208](img/GM6208.jpg)